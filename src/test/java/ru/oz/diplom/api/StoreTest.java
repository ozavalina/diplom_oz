package ru.oz.diplom.api;

import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.restassured.RestAssured;
import io.restassured.response.ResponseBodyExtractionOptions;
import org.testng.Assert;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasKey;

@Epic("API тесты")
@Feature("STORE: Access to Petstore orders")
public class StoreTest extends BaseTest {
    final int ORDER_ID = 7;
    final int WRONG_ORDER_ID = 138;

    @BeforeSuite
    public void setUpSuite() {
        RestAssured.requestSpecification = requestSpecification;
        placeOrderForPet(ORDER_ID, "approved", true);
    }

    @Test(description = "Проверка GET запроса /store/order/Id")
    public void testFindPurchaseOrderByID() {
        placeOrderForPet(ORDER_ID, "approved", true);
        RestAssured
                .get("/v2/store/order/" + ORDER_ID)
                .then()
                .spec(responseSpecification)
                .body("$", hasKey("id"))
                .body("$", hasKey("petId"))
                .body("$", hasKey("quantity"))
                .body("$", hasKey("shipDate"))
                .body("$", hasKey("status"))
                .body("$", hasKey("complete"));
    }

    @Test(description = "Проверка, что GET запрос /store/order/Id для ID>10 возвращает код 404")
    public void negativeTestFindPetByID() {
        RestAssured
                .get("/v2/store/order/" + WRONG_ORDER_ID)
                .then()
                .spec(negativeResponseSpecification)
                .statusCode(404)
                .body("code", equalTo(1))
                .body("type", equalTo("error"))
                .body("message", equalTo("Order not found"));
    }

    @Test(description = "Проверка GET запроса /store/inventory")
    public void testReturnsPetInventoriesByStatus() {
        RestAssured
                .get("/v2/store/inventory")
                .then()
                .spec(responseSpecification);
    }

    @Test(description = "Проверка POST запроса /store/order")
    public void testPlaceOrderForPet() {
        int id = 13;
        String status = "delivered";
        boolean complete = false;
        String requestBody = String.format(bodyOrder, id, status, complete);
        RestAssured
                .given()
                .contentType("application/json")
                .body(requestBody)
                .when()
                .post("/v2/store/order")
                .then()
                .spec(responseSpecification)
                .body("id", equalTo(id))
                .body("petId", equalTo(0))
                .body("quantity", equalTo(0))
                .body("shipDate", equalTo("2023-12-25T09:23:08.944" + "+0000"))
                .body("status", equalTo(status))
                .body("complete", equalTo(complete));
    }

    @Test(description = "Проверка, тела ответа на POST запрос /store/order при передаче несуществующего id")
    public void negativeAddNewPetToStoreTest() {
        int id = -1;
        String status = "delivered";
        boolean complete = false;
        String requestBody = String.format(bodyOrder, id, status, complete);
        ResponseBodyExtractionOptions responseBody = RestAssured
                .given()
                .contentType("application/json")
                .body(requestBody)
                .when()
                .post("/v2/store/order")
                .then()
                .spec(responseSpecification)
                .extract().body();
        Assert.assertNotEquals(responseBody.jsonPath().get("id"), id);
    }

    @Test(description = "Проверка DELETE запроса /pet/Id")
    public void testDeletesPet() {
        RestAssured
                .given()
                .when()
                .delete("/v2/store/order/" + ORDER_ID)
                .then()
                .spec(responseSpecification)
                .body("code", equalTo(200))
                .body("type", equalTo("unknown"))
                .body("message", equalTo(Integer.toString(ORDER_ID)));
    }

    @Test(description = "Проверка кода ошибки после выполнения DELETE запроса /pet/Id для несуществующего Id")
    public void negativeTestDeletesPet() {
        RestAssured
                .given()
                .when()
                .delete("/v2/store/order/" + WRONG_ORDER_ID)
                .then()
                .spec(negativeResponseSpecification)
                .body("code", equalTo(404))
                .body("type", equalTo("unknown"))
                .body("message", equalTo("Order Not Found"));
    }
}

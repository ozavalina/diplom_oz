package ru.oz.diplom.api;

import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.restassured.RestAssured;
import io.restassured.response.ResponseBodyExtractionOptions;
import org.testng.Assert;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.oz.diplom.api.model.PetTagData;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasKey;

@Epic("API тесты")
@Feature("PET: Everything about your Pets")
public class PetTest extends BaseTest {
    final int PET_ID = 101011;
    final int PET_ID_FOR_DELETE = 101012;
    final int NO_PET_ID = 101010;

    @BeforeSuite
    public void setUpSuite() {
        RestAssured.requestSpecification = requestSpecification;
    }

    @Test(description = "Проверка POST запроса /pet добавления питомца")
    public void addNewPetToStoreTest() {
        String requestBody = String.format(bodyPet, PET_ID, petName);
        RestAssured
                .given()
                .contentType("application/json")
                .body(requestBody)
                .when()
                .post("/v2/pet")
                .then()
                .spec(responseSpecification)
                .body("id", equalTo(PET_ID));
    }

    @Test(description = "Проверка, что POST запрос /pet при некорректном body возвращает ошибку 500")
    public void negativeTestForAddingNewPetToStore() {
        String requestBody = "123";
        RestAssured
                .given()
                .contentType("application/json")
                .body(requestBody)
                .when()
                .post("/v2/pet")
                .then()
                .spec(negativeResponseSpecification)
                .statusCode(500)
                .body("code", equalTo(500))
                .body("type", equalTo("unknown"))
                .body("message", equalTo("something bad happened"));
    }

    @Test(description = "Проверка, что GET запрос /pet/Id по несуществующему Id питомца возвращает код 404 и ошибку")
    public void negativeTestForFindingPetByID() {
        RestAssured
                .get("/v2/pet/" + NO_PET_ID)
                .then()
                .spec(negativeResponseSpecification)
                .statusCode(404)
                .body("code", equalTo(1))
                .body("type", equalTo("error"))
                .body("message", equalTo("Pet not found"));
    }

    @Test(description = "Проверка GET запроса /pet/Id по существующему Id питомца")
    public void testFindPetByID() {
        RestAssured
                .get("/v2/pet/" + PET_ID)
                .then()
                .spec(responseSpecification)
                .assertThat()
                .body("$", hasKey("id"))
                .body("$", hasKey("category"))
                .body("$", hasKey("name"))
                .body("$", hasKey("photoUrls"))
                .body("$", hasKey("tags"))
                .body("$", hasKey("status"));
    }

    @DataProvider(name = "providerGetId")
    public Object[][] dataProviderGetId() {
        return new Object[][]{
                {1, "tag1"},
                {2, "tag2"}
        };
    }

    @Test(
            description = "Проверка, что в ответе на GET /pet/Id запрос содержится информация по тэгам",
            dataProvider = "providerGetId"
    )
    public void testTagsAreContainedInResponse(Integer id, String name) {

        PetTagData pt = new PetTagData(id, name);
        PetTagData petTagData = RestAssured
                .get("/v2/pet/" + PET_ID)
                .then()
                .spec(responseSpecification)
                .extract().body().jsonPath().getObject(String.format("tags[%d]", id - 1), PetTagData.class);
        Assert.assertEquals(pt, petTagData);
    }

    @DataProvider(name = "providerGetStatus")
    public Object[] dataProviderGetStatus() {
        return new Object[]{"available", "sold"};
    }

    @Test(
            description = "Проверка GET запроса /pet/findByStatus отбора питомцев по статусу",
            dataProvider = "providerGetStatus"
    )
    public void testFindByStatus(String status) {
        RestAssured
                .given()
                .queryParam("status", status)
                .when()
                .get("/v2/pet/findByStatus")
                .then()
                .spec(responseSpecification)
                .body("[0].status", equalTo(status));
    }

    @Test(description = "Проверка, что в GET запросе /pet/findByStatus при ошибочном статусе тело ответа - '[]'")
    public void emptyResponseBodyIsReturnedTest() {
        String wrongStatus = "";
        String responseString = RestAssured
                .given()
                .queryParam("status", wrongStatus)
                .when()
                .get("/v2/pet/findByStatus")
                .then()
                .spec(responseSpecification)
                .extract().body().asString();
        Assert.assertEquals(responseString, "[]");
    }

    @Test(description = "Проверка DELETE запроса /pet/Id")
    public void testDeletesPet() {
        addNewPetToStore(PET_ID_FOR_DELETE);
        String PetIdForDeleteToString = Integer.toString(PET_ID_FOR_DELETE);
        RestAssured
                .given()
                .when()
                .delete("/v2/pet/" + PET_ID_FOR_DELETE)
                .then()
                .spec(responseSpecification)
                .body("code", equalTo(200))
                .body("type", equalTo("unknown"))
                .body("message", equalTo(PetIdForDeleteToString));
    }

    @Test(description = "Проверка кода ошибки после выполнения DELETE запроса /pet/Id для несуществующего Id")
    public void negativeTestDeletesPet() {
        RestAssured
                .given()
                .when()
                .delete("/v2/pet/" + NO_PET_ID)
                .then()
                .log().all()
                .statusCode(404)
                .header("Access-Control-Allow-Headers", "Content-Type, api_key, Authorization");
    }

    @Test(description = "Проверка ответа PUT запроса /pet")
    public void testUpdateExistingPet() {
        String newPetName = faker.cat().name();
        String requestBody = String.format(bodyPet, PET_ID, newPetName);
        ResponseBodyExtractionOptions body = RestAssured
                .given()
                .contentType("application/json")
                .body(requestBody)
                .when()
                .put("/v2/pet")
                .then()
                .spec(responseSpecification)
                .body("$", hasKey("id"))
                .body("$", hasKey("category"))
                .body("$", hasKey("name"))
                .body("$", hasKey("photoUrls"))
                .body("$", hasKey("tags"))
                .body("$", hasKey("status"))
                .extract().body();
        Assert.assertEquals(newPetName, body.jsonPath().get("name"));

        RestAssured
                .get("/v2/pet/" + PET_ID)
                .then()
                .body("name", equalTo(newPetName));
    }

    @Test(description = "Проверка кода ошибки после выполнения PUT запроса /pet для несуществующего Id")
    public void negativeTestUpdateExistingPet() {
        String errorBody = "";
        RestAssured
                .given()
                .contentType("application/json")
                .body(errorBody)
                .when()
                .put("/v2/pet")
                .then()
                .spec(negativeResponseSpecification)
                .statusCode(405)
                .body("code", equalTo(405))
                .body("type", equalTo("unknown"))
                .body("message", equalTo("no data"));
    }

    @Test(description = "Проверка POST запроса /pet/Id по существующему Id питомца")
    public void testForUpdatesPet() {
        String requestBody = String.format(bodyPet, PET_ID, petName);
        RestAssured
                .given()
                .contentType("application/json")
                .body(requestBody)
                .when()
                .post("/v2/pet");

        RestAssured
                .given()
                .queryParam("name", "newName")
                .when()
                .post("/v2/pet/" + PET_ID)
                .then()
                .spec(responseSpecification)
                .body("code", equalTo(200))
                .body("type", equalTo("unknown"))
                .body("message", equalTo(Integer.toString(PET_ID)));
    }

    @Test(description = "Проверка POST запроса /pet/Id при несуществующем ID")
    public void negativeTestForUpdatesPet() {
        RestAssured
                .given()
                .contentType("application/json")
                .body("")
                .when()
                .post("/v2/pet/" + NO_PET_ID)
                .then()
                .spec(responseSpecificationXml)
                .body("apiResponse.type", equalTo("unknown"));
    }
}

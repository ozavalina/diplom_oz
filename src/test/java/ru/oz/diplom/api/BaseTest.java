package ru.oz.diplom.api;

import com.github.javafaker.Faker;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;

import static io.restassured.http.ContentType.JSON;

public class BaseTest {
    RequestSpecification requestSpecification = new RequestSpecBuilder()
            .log(LogDetail.ALL)
            .setBaseUri("https://petstore.swagger.io")
            .build();

    ResponseSpecification responseSpecification = new ResponseSpecBuilder()
            .log(LogDetail.ALL)
            .expectContentType(JSON)
            .expectStatusCode(200)
            .expectHeader("Content-Type", "application/json")
            .build();

    ResponseSpecification negativeResponseSpecification = new ResponseSpecBuilder()
            .log(LogDetail.ALL)
            .expectContentType(JSON)
            .expectHeader("Content-Type", "application/json")
            .build();

    ResponseSpecification responseSpecificationXml = new ResponseSpecBuilder()
            .log(LogDetail.ALL)
            .expectHeader("Content-Type", "application/xml")
            .build();

    Faker faker = new Faker();
    String petName = faker.cat().name();
    String bodyPet = "{\"id\":%d,\"category\":{\"id\":1,\"name\":\"Наименование категории\"},\"name\":\"%s\",\"photoUrls\":[\"https://www.fonstola.ru/pic/201910/1680x1050/fonstola.ru_352135.jpg\"],\"tags\":[{\"id\":1,\"name\":\"tag1\"},{\"id\":2,\"name\":\"tag2\"}],\"status\":\"sold\"}";

    public void addNewPetToStore(int id) {
        String requestBody = String.format(bodyPet, id, petName);
        RestAssured
                .given()
                .contentType("application/json")
                .body(requestBody)
                .when()
                .post("/v2/pet")
                .then()
                .spec(responseSpecification);
    }

    String bodyOrder = "{\"id\":%d,\"petId\":0,\"quantity\":0,\"shipDate\":\"2023-12-25T09:23:08.944\",\"status\":\"%s\",\"complete\":%b}";

    public void placeOrderForPet(int id, String status, boolean complete) {
        String requestBody = String.format(bodyOrder, id, status, complete);
        RestAssured
                .given()
                .contentType("application/json")
                .body(requestBody)
                .when()
                .post("/v2/store/order");
    }
}

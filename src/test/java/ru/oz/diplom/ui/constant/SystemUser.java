package ru.oz.diplom.ui.constant;

/**
 * Данные для авторизации в системе
 */
public enum SystemUser {
    ADMINISTRATOR("Admin", "admin123");

    public final String username;
    public final String password;

    SystemUser(String username, String password) {
        this.username = username;
        this.password = password;
    }
}

package ru.oz.diplom.ui.constant;

public enum ButtonName {
    SEARCH(" Search "),
    RESET(" Reset "),
    ADD(" Add ");
    public final String buttonName;

    ButtonName(String buttonName) {
        this.buttonName = buttonName;
    }
}

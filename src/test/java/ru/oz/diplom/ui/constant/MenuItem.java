package ru.oz.diplom.ui.constant;

/**
 * Пункты бокового меню
 */
public enum MenuItem {
    ADMIN("Admin"),
    PIM("PIM"),
    BUZZ("Buzz"),
    CLAIM("Claim");

    public final String menuItemName;

    MenuItem(String menuItemName) {
        this.menuItemName = menuItemName;
    }
}

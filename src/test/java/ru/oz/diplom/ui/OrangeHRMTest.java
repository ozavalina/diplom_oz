package ru.oz.diplom.ui;

import com.codeborne.selenide.Configuration;
import com.github.javafaker.Faker;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Map;

import static com.codeborne.selenide.Selenide.sleep;
import static ru.oz.diplom.ui.constant.ButtonName.*;
import static ru.oz.diplom.ui.constant.MenuItem.*;
import static ru.oz.diplom.ui.constant.SystemUser.ADMINISTRATOR;

@Epic("UI тесты")
@Feature("Тесты раздела Admin")
public class OrangeHRMTest implements PageObjectSupplier {
    Faker faker = new Faker();
    String username = ADMINISTRATOR.username;
    String firstName;
    String middleName;
    String lastName;
    String postText = "I don't think, I write";

    @BeforeSuite
    public void setUpSuite() {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("selenoid:options", Map.of("enableVNC", true));

        Configuration.remote = "http://51.250.123.179:4444/wd/hub/";
        Configuration.browserSize = "1920x1080";
        Configuration.browserCapabilities = capabilities;
    }

    @BeforeTest
    public void setUpTest() {
        loginPage().openWebsite();
        loginPage().loginUser(ADMINISTRATOR);
    }

    @Test(description = "Поиск пользователя по Username")
    public void userSearchTest() {
        menuPage().clickMenuItem(ADMIN.menuItemName);
        String numberOfRecords = "(1) Record Found";
        int expectedSize = 1;
        adminPage().setUserName(username);
        adminPage().clickButton(SEARCH);
        adminPage().checkNumberOfRecordsFound(numberOfRecords, expectedSize);
        adminPage().checkUsernameInCell(username);
    }

    @Test(description = "Проверка сброса значения поля UserName для поиска")
    public void clearSearchFieldsTest() {
        menuPage().clickMenuItem(ADMIN.menuItemName);
        adminPage().setUserName(username);
        adminPage().clickButton(SEARCH);
        adminPage().clickButton(RESET);
        adminPage().checkFieldIsEmpty();
    }

    @DataProvider(name = "provider")
    public Object[][] dataProvider() {
        return new Object[][]{
                {"1234567", "Your password must contain minimum 1 lower-case letter"},
                {"123", "Should have at least 7 characters"}
        };
    }

    @Test(
            description = "Проверить сообщение об ошибке при вводе пароля, не соответствующего политике безопасности",
            dataProvider = "provider"
    )
    public void passwordErrorTest(String password, String errorText) {
        menuPage().clickMenuItem(ADMIN.menuItemName);
        adminPage().clickButton(ADD);
        addUserPage().setField(password, "Password");
        addUserPage().checkErrorText(errorText);
    }

    @Test(description = "Проверка открытия страницы Edit User при нажатии на кнопку редактирования")
    public void titlePageIsOpen() {
        menuPage().clickMenuItem(ADMIN.menuItemName);
        adminPage().setUserName(username);
        adminPage().clickButton(SEARCH);
        adminPage().selectSystemUserCheckbox();
        adminPage().clickEditButton();
        adminPage().checkEditUserPage();
    }

    @Test(description = "Проверка добавления пользователя")
    public void addUserTest() {
        menuPage().clickMenuItem(ADMIN.menuItemName);
        String newUsername = faker.name().username();
        String passwordAndConfirm = "Qa12345";
        String employeeName = "John  Smith";

        adminPage().clickButton(ADD);
        addUserPage().setFields(username, "Enabled", newUsername, passwordAndConfirm, employeeName);
        addUserPage().saveButtonClick();

        adminPage().setUserName(newUsername);
        adminPage().clickButton(SEARCH);
    }

    @Test(description = "Проверка добавления сотрудника")
    public void addEmployeeTest() {
        menuPage().clickMenuItem(PIM.menuItemName);
        firstName = faker.name().firstName();
        middleName = faker.name().firstName();
        lastName = faker.name().lastName();

        employeeInformationPage().clickButton(ADD);
        addEmployeePage().setFields(firstName, middleName, lastName);
        addEmployeePage().saveButtonClick();
        menuPage().clickMenuItem(PIM.menuItemName);
        employeeInformationPage().setEmployeeName(firstName + " " + middleName + " " + lastName);
        employeeInformationPage().clickButton(SEARCH);
        employeeInformationPage().checkFirstAndMiddleNameInCell(firstName, middleName);
    }

    @Test(description = "Проверка удаления элемента")
    public void deleteEmployeeTest() {
        menuPage().clickMenuItem(PIM.menuItemName);
        firstName = faker.name().firstName();
        middleName = faker.name().firstName();
        lastName = faker.name().lastName();

        employeeInformationPage().clickButton(ADD);
        addEmployeePage().setFields(firstName, middleName, lastName);
        addEmployeePage().saveButtonClick();
        menuPage().clickMenuItem(PIM.menuItemName);
        employeeInformationPage().setEmployeeName(firstName + " " + middleName + " " + lastName);
        employeeInformationPage().clickButton(SEARCH);
        employeeInformationPage().clickDeleteIcon();
        employeeInformationPage().checkTableIsEmpty();
    }

    @Test(description = "Проверка сброса значения поля Employee Id для поиска")
    public void clearEmployeeIdFieldTest() {
        menuPage().clickMenuItem(PIM.menuItemName);
        String employeeId = "123456";
        employeeInformationPage().setField(employeeId, "Employee Id");
        employeeInformationPage().clickButton(RESET);
        employeeInformationPage().checkFieldIsEmpty("Employee Id");
    }

    @Test(description = "Проверка, что поля First Name и Last Name обязательные")
    public void fieldRequirementTest() {
        menuPage().clickMenuItem(PIM.menuItemName);
        firstName = faker.name().firstName();
        middleName = faker.name().firstName();
        lastName = faker.name().lastName();

        employeeInformationPage().clickButton(ADD);
        addEmployeePage().setFields(firstName, middleName, lastName);
        addEmployeePage().saveButtonClick();
        addEmployeePage().clearAndCheckFields();
    }

    @Test(description = "Проверить публикацию поста")
    public void postPublishingTest() {
        menuPage().clickMenuItem(BUZZ.menuItemName);
        buzzPage().setField(postText);
        buzzPage().clickButton();
        buzzPage().checkPost(postText);
    }

    @Test(description = "Удаление поста")
    public void deletePostTest() {
        String newPostText = "Please do not shoot the piano player; he is doing his best.";
        menuPage().clickMenuItem(BUZZ.menuItemName);
        buzzPage().setField(newPostText);
        buzzPage().clickButton();
        buzzPage().deletePost();
        buzzPage().checkPostTextIsAbsent(newPostText);
    }

    @Test(description = "Добавление комментария")
    public void addCommentTest() {
        String textComment = "Мой комментарий 123";
        menuPage().clickMenuItem(BUZZ.menuItemName);
        buzzPage().setField(postText);
        buzzPage().clickButton();
        buzzPage().addComment(textComment);
    }

    @Test(description = "Отмена удаления поста")
    public void cancelCommentPost() {
        String newPostText = "newPostText";
        menuPage().clickMenuItem(BUZZ.menuItemName);
        buzzPage().setField(newPostText);
        buzzPage().clickButton();
        buzzPage().cancelCommentPost();
        buzzPage().checkPost(newPostText);
    }

    @Test(description = "Проверка ввода несуществующего значения")
    public void TestInputOfNonExistentValue() {
        menuPage().clickMenuItem(CLAIM.menuItemName);
        String employeeName = "Бонифаций";
        claimPage().checkEntryOfNonExistentValue(employeeName);
    }

    @Test(description = "Проверка сброса значения поля Employee Name для поиска")
    public void clearEmployeeNameFieldTest() {
        menuPage().clickMenuItem(CLAIM.menuItemName);
        String employeeName = "Бонифаций";
        sleep(1000);
        claimPage().checkEntryOfNonExistentValue(employeeName);
        claimPage().clickButton(RESET);
        claimPage().checkNoMessageAboutIncorrectInput();
    }
}

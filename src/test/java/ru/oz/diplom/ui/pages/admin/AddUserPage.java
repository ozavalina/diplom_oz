package ru.oz.diplom.ui.pages.admin;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.sleep;

public class AddUserPage {
    SelenideElement employeeNameField = $x("//input[@placeholder='Type for hints...']");
    SelenideElement errorTextField = $x("//span[@class='oxd-text oxd-text--span oxd-input-field-error-message oxd-input-group__message']");
    SelenideElement saveButton = $x("//button[@type='submit']");
    String fieldXPathTemplate = "//label[text()='%s']/parent::*//following-sibling::div/input";
    String comboboxXPathTemplate = "//label[text()='%s']/parent::*//following-sibling::div//i";
    String itemComboboxXPathTemplate = "//div[@role='option']/span[text()='%s']";

    //setTimeout(()=>{debugger;},50000);

    @Step("Проверить текст предупреждения")
    public void checkErrorText(String errorText) {
        errorTextField.shouldHave(text(errorText));
    }

    @Step("Нажать кнопку выпадашки")
    public void clickCombobox(String combobox) {
        $x(String.format(comboboxXPathTemplate, combobox)).click();
    }

    @Step("Выбрать пункт из списка {1} из {0}")
    public void selectValue(String combobox, String comboboxItem) {
        clickCombobox(combobox);
        $x(String.format(itemComboboxXPathTemplate, comboboxItem)).click();
    }

    @Step("Ввести значение {0} в поле {1}")
    public void setField(String value, String field) {
        $x(String.format(fieldXPathTemplate, field)).sendKeys(value);
    }

    @Step("Ввести Employee Name")
    public void setEmployeeName(String value) {
        employeeNameField.sendKeys(value);
        $x(String.format(itemComboboxXPathTemplate, value)).click();
    }

    @Step("Заполнить все поля формы")
    public void setFields(String valueUserRole, String valueStatus, String username, String passwordAndConfirm, String employeeName) {
        selectValue("User Role", valueUserRole);
        selectValue("Status", valueStatus);
        setField(username, "Username");
        setField(passwordAndConfirm, "Password");
        setField(passwordAndConfirm, "Confirm Password");
        setEmployeeName(employeeName);
    }

    @Step("Нажать кнопку Save")
    public void saveButtonClick() {
        saveButton.click();
        sleep(1000);
    }
}

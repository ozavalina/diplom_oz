package ru.oz.diplom.ui.pages;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import ru.oz.diplom.ui.constant.SystemUser;

import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.open;

public class LoginPage {
    SelenideElement userNameField = $x("//input[@name='username']");
    SelenideElement passwordField = $x("//input[@name='password']");
    SelenideElement loginButton = $x("//button[text()=' Login ']");

    @Step("Открыть страницу веб-сайта")
    public void openWebsite() {
        open("https://opensource-demo.orangehrmlive.com");
    }

    @Step("Ввести имя пользователя")
    public void setUserName(String username) {
        userNameField.sendKeys(username);
    }

    @Step("Ввести пароль")
    public void setPassword(String password) {
        passwordField.sendKeys(password);
    }

    @Step("Нажать кнопку Login")
    public void clickLoginButton() {
        loginButton.click();
    }

    @Step("Авторизоваться на веб-сайте")
    public void loginUser(SystemUser user) {
        setUserName(user.username);
        setPassword(user.password);
        clickLoginButton();
    }

}

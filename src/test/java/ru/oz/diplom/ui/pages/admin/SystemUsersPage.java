package ru.oz.diplom.ui.pages.admin;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import ru.oz.diplom.ui.constant.ButtonName;

import static com.codeborne.selenide.CollectionCondition.size;
import static com.codeborne.selenide.Condition.empty;
import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.$x;

public class SystemUsersPage {
    SelenideElement userNameField = $x("//div[@class='oxd-table-filter-area']//input[@class='oxd-input oxd-input--active']");
    SelenideElement oxdTable = $x("//div[@class='oxd-table']");
    String buttonNameXpathTemplate = "//button[text()='%s']";
    SelenideElement numberOfRecordsText = $x("//div[@class='orangehrm-horizontal-padding orangehrm-vertical-padding']/span");
    SelenideElement usernameCell = $x("//div[@class='oxd-table']//div[@role='cell'][2]//child::div");
    SelenideElement systemUserCheckbox = $x("//input[@type='checkbox']/following-sibling::span");
    SelenideElement editButton = $x("//button[@class='oxd-icon-button oxd-table-cell-action-space'][2]");
    SelenideElement pageTitle = $x("//div[@class='orangehrm-card-container']/h6");

    @Step("Ввести имя пользователя {username} в поле Username")
    public void setUserName(String username) {
        userNameField.sendKeys(username);
    }

    @Step("Нажать кнопку {0}")
    public void clickButton(ButtonName buttonName) {
        $x(String.format(buttonNameXpathTemplate, buttonName.buttonName)).click();
    }

    @Step("Проверить количество найденных записей")
    public void checkNumberOfRecordsFound(String numberOfRecords, int expectedSize) {
        numberOfRecordsText.shouldHave(text(numberOfRecords));
        oxdTable.findAll(".oxd-table-card").shouldHave(size(expectedSize));
    }

    @Step("Проверить, что в ячейке имя пользователя {0}")
    public void checkUsernameInCell(String username) {
        usernameCell.shouldHave(text(username));
    }

    @Step("Проверить, что поле userName пустое")
    public void checkFieldIsEmpty() {
        userNameField.shouldBe(empty);
    }

    @Step("Отметить чек-бокс у выбранного пользователя")
    public void selectSystemUserCheckbox() {
        systemUserCheckbox.click();
    }

    @Step("Нажать кнопку редактирования пользователя")
    public void clickEditButton() {
        editButton.click();
    }

    @Step("Проверить, что открыта страница с заголовком Edit User")
    public void checkEditUserPage() {
        pageTitle.shouldHave(text("Edit User"));
    }
}

package ru.oz.diplom.ui.pages.buzz;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.sleep;

public class BuzzPage {
    SelenideElement postTextField = $x("//textarea");
    SelenideElement postField = $x("(//p[@class='oxd-text oxd-text--p orangehrm-buzz-post-body-text'])[1]");
    SelenideElement postButton = $x("//button[@type='submit']");
    SelenideElement pointsIcon = $x("(//button[@class='oxd-icon-button'])[2]");
    SelenideElement commentsIcon = $x("(//button[@class='oxd-icon-button'])[3]");
    SelenideElement commentField = $x("//input[@placeholder='Write your comment...']");
    SelenideElement commentsField = $x("//div[@class='orangehrm-buzz-comment']");
    SelenideElement deletePost = $x("//p[text()='Delete Post']");
    SelenideElement yesButton = $x("//div[@role='document']//button[@type='button']/i");
    SelenideElement noButton = $x("//div[@role='document']//button[text()=' No, Cancel ']");

    @Step("Заполнить текст поста")
    public void setField(String value) {
        postTextField.sendKeys(value);
    }

    @Step("Нажать Post")
    public void clickButton() {
        postButton.click();
        sleep(1000);
    }

    @Step("Проверить, что введенный текст соответствует тексту в посте")
    public void checkPost(String value) {
        postField.shouldHave(text(value));
    }

    @Step("Удалить пост")
    public void deletePost() {
        pointsIcon.click();
        deletePost.click();
        yesButton.click();
    }

    @Step("Отменить удаление поста")
    public void cancelCommentPost() {
        pointsIcon.click();
        deletePost.click();
        noButton.click();
    }

    @Step("Проверить, что текст поста отсутствует")
    public void checkPostTextIsAbsent(String value) {
        sleep(1000);
        postField.shouldNotHave(text(value));
    }

    @Step("Добавить комментарий")
    public void addComment(String value) {
        commentsIcon.click();
        commentField.sendKeys(value);
        commentField.pressEnter();
        sleep(2000);
        commentsField.shouldHave(text(value));
    }
}

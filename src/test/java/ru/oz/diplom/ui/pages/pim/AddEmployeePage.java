package ru.oz.diplom.ui.pages.pim;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.openqa.selenium.Keys;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.sleep;

public class AddEmployeePage {
    SelenideElement firstNameField = $x("//input[@placeholder='First Name']");
    SelenideElement middleNameField = $x("//input[@placeholder='Middle Name']");
    SelenideElement lastNameField = $x("//input[@placeholder='Last Name']");
    SelenideElement firstNameRequiredText = $x("//input[@placeholder='First Name']//parent::*//following-sibling::span[@class='oxd-text oxd-text--span oxd-input-field-error-message oxd-input-group__message']");
    SelenideElement lastNameRequiredText = $x("//input[@placeholder='Last Name']//parent::*//following-sibling::span[@class='oxd-text oxd-text--span oxd-input-field-error-message oxd-input-group__message']");
    SelenideElement saveButton = $x("//button[@type='submit']");

    @Step("Заполнить все поля формы")
    public void setFields(String firstName, String middleName, String lastName) {
        firstNameField.sendKeys(firstName);
        middleNameField.sendKeys(middleName);
        lastNameField.sendKeys(lastName);
    }

    @Step("Очистить поля first Name и last Name и проверить их обязательность")
    public void clearAndCheckFields() {
        sleep(4000);
        firstNameField.sendKeys(Keys.CONTROL + "A");
        firstNameField.sendKeys(Keys.BACK_SPACE);
        lastNameField.sendKeys(Keys.CONTROL + "A");
        lastNameField.sendKeys(Keys.BACK_SPACE);
        firstNameRequiredText.shouldHave(visible, text("Required"));
        lastNameRequiredText.shouldHave(visible, text("Required"));
    }

    @Step("Нажать кнопку Save")
    public void saveButtonClick() {
        saveButton.click();
        sleep(1000);
    }
}

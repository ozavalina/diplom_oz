package ru.oz.diplom.ui.pages;

import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.sleep;

public class MenuPage {
    String menuItemXpathTemplate = "//span[text()='%s']";

    @Step("Нажать пункт меню {menuItemName}")
    public void clickMenuItem(String menuItemName) {
        $x(String.format(menuItemXpathTemplate, menuItemName)).click();
        sleep(2000);
    }
}

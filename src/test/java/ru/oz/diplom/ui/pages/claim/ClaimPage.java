package ru.oz.diplom.ui.pages.claim;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import ru.oz.diplom.ui.constant.ButtonName;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$x;

public class ClaimPage {
    SelenideElement employeeNameField = $x("//label[text()='Employee Name']/parent::*//following-sibling::div//input");
    SelenideElement referenceIdField = $x("//label[text()='Reference Id']/parent::*//following-sibling::div//input");
    SelenideElement employeeNameFieldText = $x("//input[@placeholder='Type for hints...']//parent::*/parent::*/parent::*//parent::*//span");
    String buttonNameXpathTemplate = "//button[text()='%s']";

    @Step("Ввести несуществующее Employee Name и проверить поле")
    public void checkEntryOfNonExistentValue(String value) {
        employeeNameField.sendKeys(value);
        referenceIdField.click();
        employeeNameFieldText.shouldHave(visible, text("Invalid"));
    }

    @Step("Нажать кнопку {0}")
    public void clickButton(ButtonName buttonName) {
        $x(String.format(buttonNameXpathTemplate, buttonName.buttonName)).click();
    }

    @Step("Проверить, что сообщение о некорректном вводе отсутствует")
    public void checkNoMessageAboutIncorrectInput() {
        employeeNameFieldText.shouldNotBe(visible);
    }
}

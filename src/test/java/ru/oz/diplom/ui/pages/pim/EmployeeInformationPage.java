package ru.oz.diplom.ui.pages.pim;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import ru.oz.diplom.ui.constant.ButtonName;

import static com.codeborne.selenide.Condition.empty;
import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.$x;

public class EmployeeInformationPage {
    String buttonNameXpathTemplate = "//button[text()='%s']";
    SelenideElement employeeNameField = $x("//input[@placeholder='Type for hints...']");
    SelenideElement firstAndMiddleNameCell = $x("//div[@class='oxd-table-body']//div[@role='cell'][3]//child::div");
    SelenideElement trashButton = $x("//div[@role='cell']//button/i[@class='oxd-icon bi-trash']");
    SelenideElement yesButton = $x("//div[@role='document']//button[@type='button']/i");
    SelenideElement table = $x("//div[@class='oxd-table-body']");
    String itemComboboxXPathTemplate = "//div[@role='option']/span[text()='%s']";
    String fieldXPathTemplate = "//label[text()='%s']/parent::*//following-sibling::div/input";


    @Step("Нажать кнопку {0}")
    public void clickButton(ButtonName buttonName) {
        $x(String.format(buttonNameXpathTemplate, buttonName.buttonName)).click();
    }

    @Step("Нажать иконку удаления")
    public void clickDeleteIcon() {
        trashButton.click();
        yesButton.click();
    }

    @Step("Проверить, что таблица пустая")
    public void checkTableIsEmpty() {
        table.shouldBe(empty);
    }

    @Step("Ввести значение {0} в поле {1}")
    public void setField(String value, String field) {
        $x(String.format(fieldXPathTemplate, field)).sendKeys(value);
    }

    @Step("Проверить, что поле Employee Id пустое")
    public void checkFieldIsEmpty(String field) {
        $x(String.format(fieldXPathTemplate, field)).shouldBe(empty);
    }

    @Step("Заполнить поле Employee Name")
    public void setEmployeeName(String fullName) {
        employeeNameField.sendKeys(fullName);
        $x(String.format(itemComboboxXPathTemplate, fullName)).click();
    }

    @Step("Проверить, что в гриде отобразился добавленный сотрудник")
    public void checkFirstAndMiddleNameInCell(String firstName, String middleName) {
        firstAndMiddleNameCell.shouldHave(text(firstName + " " + middleName));
    }
}

package ru.oz.diplom.ui;

import ru.oz.diplom.ui.pages.LoginPage;
import ru.oz.diplom.ui.pages.MenuPage;
import ru.oz.diplom.ui.pages.admin.AddUserPage;
import ru.oz.diplom.ui.pages.admin.SystemUsersPage;
import ru.oz.diplom.ui.pages.buzz.BuzzPage;
import ru.oz.diplom.ui.pages.claim.ClaimPage;
import ru.oz.diplom.ui.pages.pim.AddEmployeePage;
import ru.oz.diplom.ui.pages.pim.EmployeeInformationPage;

public interface PageObjectSupplier {
    default LoginPage loginPage() {
        return new LoginPage();
    }

    default MenuPage menuPage() {
        return new MenuPage();
    }

    default SystemUsersPage adminPage() {
        return new SystemUsersPage();
    }

    default AddUserPage addUserPage() {
        return new AddUserPage();
    }

    default EmployeeInformationPage employeeInformationPage() {
        return new EmployeeInformationPage();
    }

    default AddEmployeePage addEmployeePage() {
        return new AddEmployeePage();
    }

    default BuzzPage buzzPage() {
        return new BuzzPage();
    }


    default ClaimPage claimPage() {
        return new ClaimPage();
    }
}

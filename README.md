## Описание проекта

Проект содержит два пакета: **api** и **ui**.

В пакете **api** аходятся api-тесты на *https://petstore.swagger.io/*.

В пакете **ui** находятся ui-тесты на *https://opensource-demo.orangehrmlive.com*.

## Запуск наборов автотестов и просмотр отчёта в Allure

Выполнить в терминале или в Run Anything (Ctrl+Ctrl) одну из команд:

1. Для запуска всех тестов (api+ui): *mvn clean test allure:report allure:serve*

2. Для запуска только api-тестов: *mvn clean test -DsuiteXmlFile=api.xml allure:report allure:serve*

3. Для запуска только ui-тестов: *mvn clean test -DsuiteXmlFile=ui.xml allure:report allure:serve*

Отчет в Allure открывается в браузере после выполнения одной из вышеперечисленных команд

## Запуск отдельного автотеста и его просмотр в Selenoid

1. Открыть ссылку на Selenoid *http://51.250.123.179:8080/#/*
2. Выбрать тест, который необходимо запустить, и нажать *Ctrl+Shift+F10*
3. Открыть появившуюся сессию в Selenoid

## Запуск автотеста и формирования отчета в Allure через меню Maven (двойной клик по команде)

1. Lifecycle: *clean*
2. Lifecycle: *test*
3. Plugins/allure: *allure:report*
4. Plugins/allure: *allure:serve*

### Отчет Allure в CI/CD возможно просмотреть, открыв пайплайн внутри джобы кнопка Browse //artifacts/browse/target/site/allure-maven-plugin/index.html, далее пройти по ссылке.
